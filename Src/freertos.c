#include <sched.h>
/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "commuTask.h"
#include "motorTask.h"
#include "HighPriority.h"
#include "gdtr_define.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */





/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
osThreadId_t commuTask_Id;
osThreadId_t motorTask_Id;
osThreadId_t highPriority_ID;
osMessageQueueId_t msgMotorInfo_id;
osMessageQueueId_t msgLogInfo_id;
osSemaphoreId smpMotorReady_id;

const osMessageQueueAttr_t msgMotorInfo_attr = {
  .name = "MotorInfo",
  .cb_mem = NULL,
  .cb_size = 0,
  .mq_mem = NULL,
  .mq_size = 0,
  .attr_bits = 0,
};


const osMessageQueueAttr_t msgLogInfo_attr = {
  .name = "LogInfo",
  .cb_mem = NULL,
  .cb_size = 0,
  .mq_mem = NULL,
  .mq_size = 0,
  .attr_bits = 0,
};

const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};

const osThreadAttr_t motorTask_attr = {
        .name = "motorTask",
        .priority = (osPriority_t) osPriorityAboveNormal,
        .stack_size = 128 * 8
};

const osThreadAttr_t communicate_attr = {
        .name = "communicateTask",
        .stack_size = 128 * 8,
        .priority = (osPriority_t) osPriorityNormal
};



const osThreadAttr_t highPriority_attr = {
        .name = "highPriority",
        .priority = (osPriority_t) osPriorityHigh,
        .stack_size = 128 * 8
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

_Noreturn /* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}
void usartPrint1(char byte)
{
    while((UART4->SR&0X40)==0);//循环发送,直到发送完毕
    UART4->DR = (uint8_t) byte;
}


/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
_Noreturn /* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
      msgMotorInfo_id = osMessageQueueNew(10,sizeof(msgMotorInfoStructure),&msgMotorInfo_attr);
      msgLogInfo_id = osMessageQueueNew(5,sizeof(char*),&msgLogInfo_attr);
      smpMotorReady_id = osSemaphoreNew(2,0,NULL);

      highPriority_ID = osThreadNew(highPriority,NULL,&highPriority_attr);
      motorTask_Id = osThreadNew(motorTaskFunc,NULL,&motorTask_attr);
      commuTask_Id = osThreadNew(commuTaskFunc,NULL,&communicate_attr);

      vTaskDelete(defaultTaskHandle);
      osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
