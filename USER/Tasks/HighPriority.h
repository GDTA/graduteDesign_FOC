//
// Created by 74354 on 2022/1/18.
//

#ifndef MYODRIVE_HIGHPRIORITY_H
#define MYODRIVE_HIGHPRIORITY_H

#include "main.h"


#ifdef __cplusplus
extern "C"
{
#endif

void highPriority(void *argument);

#ifdef __cplusplus
}
#endif



#endif //MYODRIVE_HIGHPRIORITY_H
