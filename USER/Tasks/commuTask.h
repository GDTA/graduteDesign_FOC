//
// Created by 74354 on 2022/1/1.
//

#ifndef MYODRIVE_COMMUTASK_H
#define MYODRIVE_COMMUTASK_H


#include "cmsis_os2.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct{
    int motor_id;
    float motorAngle;
    float I_current;
}msgMotorInfoStructure;


void commuTaskFunc(void *argument);
uint8_t sendLogMsg(osMessageQueueId_t msg_id,char** str);
#ifdef __cplusplus
}
#endif



#endif //MYODRIVE_COMMUTASK_H
