//
// Created by 74354 on 2022/1/18.
//
#include "HighPriority.h"
#include "cmsis_os.h"
#include "boardInit.h"

extern osSemaphoreId smpMotorReady_id;

void highPriority(void *argument)
{
    osSemaphoreAcquire(smpMotorReady_id,osWaitForever);
    for(;;)
    {
        
        osDelay(10);
    }
}

