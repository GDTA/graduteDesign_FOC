//
// Created by 74354 on 2022/1/2.
//

#ifndef MYODRIVE_MOTORTASK_H
#define MYODRIVE_MOTORTASK_H


#ifdef __cplusplus
extern "C"
{
#endif

void motorTaskFunc(void *argument);

#ifdef __cplusplus
}
#endif


#endif //MYODRIVE_MOTORTASK_H
