//
// Created by 74354 on 2022/1/11.
//

#ifndef MYODRIVE_FOCMATH_H
#define MYODRIVE_FOCMATH_H

#define PI          3.1415926535897932385
#define PI_Div_2    1.5707963267948966192
#define PI_Div_4    0.7853981633974483096

#define SQRT_3      1.7320508075688772935
#define SQRT_2      1.4142135623730950488
#define sqrt3_by_2  0.8660254037844386468
#define one_by_sqrt3 0.5773502691896257645


#define ABS(x) ( (x)>0?(x):-(x) )

inline void clark(float i_a,float i_b,float i_c,float& i_alpha,float& i_beta)
//{i_alpha = i_a - 0.5 * i_b - 0.5 * i_c;
// i_beta  = SQRT_3 * (i_b - i_c) / 2;}
{i_alpha = i_a;
 i_beta  =  (i_a + 2*i_b)/SQRT_3;}

inline void park(float i_alpha,float i_beta,float sita,float& i_d,float& i_q)
//{i_d =  i_alpha * sin(sita) - i_beta * cos(sita);
// i_q = -i_alpha * cos(sita) - i_beta * sin(sita);}
{i_d =  i_alpha * cos(sita) + i_beta * sin(sita);
 i_q = -i_alpha * sin(sita) + i_beta * cos(sita);}

inline void inv_park(float u_d,float u_q,float sita,float& u_alpha,float& u_beta)
//{u_alpha =  u_d*sin(sita) - u_q*cos(sita);
// u_beta  = -u_d*cos(sita) - u_q*sin(sita);}
{
    u_alpha =  u_d*cos(sita) - u_q*sin(sita);
    u_beta  = u_d*sin(sita) + u_q*cos(sita);
}



#endif //MYODRIVE_FOCMATH_H
