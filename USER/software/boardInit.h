//
// Created by 74354 on 2022/1/2.
//

#ifndef _MYODRIVE_BOARDINIT_H_
#define _MYODRIVE_BOARDINIT_H_

#include <type_traits>
#include "Motor.hpp"
#include "tim.h"

extern Motor_t motor0;
extern Motor_t motor1;
extern 


void init_foc();
void init_drv8301();
void init_adc_driverTimer();
void init_encoder();
void init_motor();
inline uint16_t get_M0_count(void);
inline uint16_t get_M1_count(void);
float getTimeNow();
uint32_t getTimeNowInt();

#define VOFA_TAIL_LEN 4
static uint8_t vofaDataTail[VOFA_TAIL_LEN] = {0x00,0x00,0x80,0x7f};

extern UART_HandleTypeDef huart4;
template<typename T>
void sendVofaWave(T* val,int len)
{
    if(std::is_same<float,T>::value)
    {
        uint8_t* pVal = (uint8_t*)val;
        HAL_UART_Transmit(&huart4,pVal,len * 4,100);
    }
    
    HAL_UART_Transmit(&huart4,vofaDataTail,VOFA_TAIL_LEN,100);
}

#endif //MYODRIVE_BOARDINIT_H
