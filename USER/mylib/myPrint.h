#ifndef MY_PRINT_H
#define MY_PRINT_H

#include <stdlib.h>

/*
	This is the common used printf function for embeded system
	Instruction:
	
			void send(char data)
			{
				while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
				USART1->DR = (u8) data;
			}
			
			PrintDevice* usartprint = new_PrintDevice(send);
			
			usartprint->printfln(usartprint,"myPrintf >>> 十进制:%d 八进制:%o 十六进制:%x", a, a, a);
			usartprint->printfln(usartprint,"myPrintf >>> 字符:'%c' 字符串:%s 浮点数:%f", c, s, f);
			usartprint->printfln(usartprint,"myPrintf >>> 地址:%p", p);
			
			
			// if you  want to delete this 'object'
			delete_PrintDevice(usartprint);
	
	Author:GDTR
	Last modification date:2021.11.27  1.05
	
*/

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct _PrintDevice PrintDevice;

typedef void (*fptrPrint)(PrintDevice*,char *,...);

/* print device "class" define */
typedef struct _PrintDevice{

    /* API for hardware */
    void (*sendOneByte)(const char);

    /* printf function */
    fptrPrint printf;

    /* printf with next line */
    fptrPrint printfln;
}PrintDevice;

void myPrintf(PrintDevice* ptrOBJ,char *s, ...);
void myPrintfln(PrintDevice* ptrOBJ,char *s, ...);

/* User's API*/
PrintDevice* new_PrintDevice(void (*_sendOneByte)(const char));
void delete_PrintDevice(PrintDevice* ptrOBJ);
void creat_PrintDevice(PrintDevice* device,void (*_sendOneByte)(char));

#ifdef __cplusplus
}
#endif

#endif
