#ifndef __USER_DEFINE_H__
#define __USER_DEFINE_H__

#include <stdio.h>
#include "printf.h"

#define USER_LOG_LEVEL 0B11111

typedef enum {
    USER_LOG_INFO = 1 << 0,
    USER_LOG_DEBUG = 1 << 1,
    USER_LOG_WARN = 1 << 2,
    USER_LOG_ERR = 1 << 3,
    USER_LOG_EXCEPTION = 1 << 4,
    USER_LOG_EXIT = 1 << 5,
}UserLogLevelType;

#define USER_LOG(level,fmt, ...)\
     do{if((level) & (USER_LOG_LEVEL)) printf("[%s][%d]["#level"]:" fmt "\r\n",__FUNCTION__,__LINE__, ##__VA_ARGS__); }while(0)

#define USER_LOG_SPRINTF(dst,level,fmt,...)\
    do{if((level) & (USER_LOG_LEVEL)) sprintf((dst),"[%s][%d]["#level"]:" fmt "\r\n",__FUNCTION__,__LINE__, ##__VA_ARGS__);}while(0)

#define malloc pvPortMalloc
#define free vPortFree

#endif // __DDTR_DEFINE_H__
