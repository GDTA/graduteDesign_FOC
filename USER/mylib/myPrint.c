#include "myPrint.h"
#include <stdio.h>
#include <stdarg.h>

/* This file was referenced:
		https://blog.csdn.net/zhyfxy/article/details/65635603
		https://blog.csdn.net/qq_41035588/article/details/83220749
	 Thanks!!!
*/


/* 功能函数声明 */
void printNum(PrintDevice* ptrOBJ, unsigned long num, int base); // 通用数字打印函数 
void printDeci(PrintDevice* ptrOBJ,int dec);					// 打印十进制数
void printOct(PrintDevice* ptrOBJ,unsigned oct);				// 打印八进制数
void printHex(PrintDevice* ptrOBJ,unsigned hex);				// 打印十六进制数
void printAddr(PrintDevice* ptrOBJ,unsigned long addr);			// 打印地址
void printStr(PrintDevice* ptrOBJ,char *str);					// 打印字符串
void printFloat(PrintDevice* ptrOBJ,double f);					// 打印浮点数

/*
 * name: delete_PrintDevice
 * function: delect a 'object'
 * param: 1.PrintDevice type pointer
 * return: null
*/
void delete_PrintDevice(PrintDevice* ptrOBJ)
{
	ptrOBJ->printfln(ptrOBJ,"This object is deleted");
	free(ptrOBJ);
}

/*
 * name: new_PrintDevice
 * function: new a 'object'
 * param: 1.char type pointer
 * return: null
*/
PrintDevice* new_PrintDevice(void (*_sendOneByte)(const char))
{
	PrintDevice* pPrintDevice;
	pPrintDevice = (PrintDevice*)malloc(sizeof(PrintDevice));
	pPrintDevice->sendOneByte = _sendOneByte;
	pPrintDevice->printf = myPrintf;
	pPrintDevice->printfln = myPrintfln;
	return pPrintDevice;
}

/*
 * name: creat_PrintDevice
 * function: creat a 'object' from global variar
 * param: 1.char type pointer
 *        2.PrintDevice
 * return: null
*/
void creat_PrintDevice(PrintDevice* device,void (*_sendOneByte)(char))
{
    device->sendOneByte = _sendOneByte;
    device->printf = myPrintf;
    device->printfln = myPrintfln;
}


/*
 * 函数名: myPrintfln
 * 函数功能: 打印格式字符串
 * 参数: 1. 包含格式符的字符串地址 2.可变参
 * 返回值: 无
*/
void myPrintfln(PrintDevice* ptrOBJ,char* s, ...)
{
	    int i = 0;

    /* 可变参第一步 */
    va_list va_ptr;

    /* 可变参第二部 */
    va_start(va_ptr, s);

    /* 循环打印所有格式字符串 */
    while (s[i] != '\0')
    {
        /* 普通字符正常打印 */
        if (s[i] != '%')
        {
            ptrOBJ->sendOneByte(s[i++]);
            continue;
        }

        /* 格式字符特殊处理 */
        switch (s[++i])   // i先++是为了取'%'后面的格式字符
        {
            /* 根据格式字符的不同来调用不同的函数 */
        case 'd': printDeci(ptrOBJ,va_arg(va_ptr, int));
            break;
        case 'o': printOct(ptrOBJ,va_arg(va_ptr, unsigned int));
            break;
        case 'x': printHex(ptrOBJ,va_arg(va_ptr, unsigned int));
            break;
        case 'c': ptrOBJ->sendOneByte(va_arg(va_ptr, int));
            break;
        case 'p': printAddr(ptrOBJ,va_arg(va_ptr, unsigned long));
            break;
        case 'f': printFloat(ptrOBJ,va_arg(va_ptr, double));
            break;
        case 's': printStr(ptrOBJ,va_arg(va_ptr, char*));
            break;
        default: break;
        }

        i++; // 下一个字符
    }

    /* 可变参最后一步 */
    va_end(va_ptr);
		myPrintf(ptrOBJ,"\r\n");
}

/*
 * 函数名: myPrintf
 * 函数功能: 打印格式字符串
 * 参数: 1. 包含格式符的字符串地址 2.可变参
 * 返回值: 无
*/
void myPrintf(PrintDevice* ptrOBJ,char* s, ...)
{
    int i = 0;

    /* 可变参第一步 */
    va_list va_ptr;

    /* 可变参第二部 */
    va_start(va_ptr, s);

    /* 循环打印所有格式字符串 */
    while (s[i] != '\0')
    {
        /* 普通字符正常打印 */
        if (s[i] != '%')
        {
            ptrOBJ->sendOneByte(s[i++]);
            continue;
        }

        /* 格式字符特殊处理 */
        switch (s[++i])   // i先++是为了取'%'后面的格式字符
        {
            /* 根据格式字符的不同来调用不同的函数 */
        case 'd': printDeci(ptrOBJ,va_arg(va_ptr, int));
            break;
        case 'o': printOct(ptrOBJ,va_arg(va_ptr, unsigned int));
            break;
        case 'x': printHex(ptrOBJ,va_arg(va_ptr, unsigned int));
            break;
        case 'c': ptrOBJ->sendOneByte(va_arg(va_ptr, int));
            break;
        case 'p': printAddr(ptrOBJ,va_arg(va_ptr, unsigned long));
            break;
        case 'f': printFloat(ptrOBJ,va_arg(va_ptr, double));
            break;
        case 's': printStr(ptrOBJ,va_arg(va_ptr, char*));
            break;
        default: break;
        }

        i++; // 下一个字符
    }

    /* 可变参最后一步 */
    va_end(va_ptr);
}

/*
 * 函数名: printNum()
 * 函数功能: 通用数字打印函数可以把整型值打印成
 *           10进制数,8进制数,2进制数,16进制数
 * 参数: 1.需要打印的整数,无符号长整型是为了兼容
 *         地址格式打印; 
				 2.打印的进制
 * 返回值: 无
*/
void printNum(PrintDevice* ptrOBJ,unsigned long num, int base)
{
    /* 递归结束条件 */
	
    if (num == 0)return;
    /* 继续递归 */
    printNum(ptrOBJ,num / base, base);
    /* 逆序打印结果 */
    ptrOBJ->sendOneByte("0123456789abcdef"[num % base]);
}


/*
 * 函数名: printDeci
 * 函数功能: 打印十进制数
 * 参数: 十进制整数
 * 返回值: 无
*/
void printDeci(PrintDevice* ptrOBJ,int dec)
{
    /* 处理有符号整数为负数时的情况 */
    if (dec < 0)
    {
		ptrOBJ->sendOneByte("-"[0]);
        dec = -dec;  	   // 该操作存在溢出风险:最小的负数没有对应的正数
    }

    /* 处理整数为时0的情况 */
    if (dec == 0)
    {
				ptrOBJ->sendOneByte("0"[0]);
        return;
    }
    else
    {
        printNum(ptrOBJ,dec,10); // 打印十进制数
    }
}

/*
 * 函数名: printOct
 * 函数功能: 打印八进制整数
 * 参数: 无符号整数
 * 返回值: 无
*/
void printOct(PrintDevice* ptrOBJ,unsigned oct)
{
    if (oct == 0)			// 处理整数为0的情况
    {
				ptrOBJ->sendOneByte("0"[0]);
        return;
    }
    else
    {
        printNum(ptrOBJ,oct, 8);	// 打印8进制数
    }
}

/*
 * 函数名: printHex
 * 函数功能: 打印十六进制整数
 * 参数: 无符号整数
 * 返回值: 无
*/
void printHex(PrintDevice* ptrOBJ,unsigned hex)
{
    if (hex == 0)			// 处理整数为0的情况
    {
				ptrOBJ->sendOneByte("0"[0]);
        return;
    }
    else
    {
        printNum(ptrOBJ,hex, 16);	// 打印十六进制数
    }
}

/*
 * 函数名: printAddr
 * 函数功能: 打印地址
 * 参数: 待打印的地址
 * 返回值: 无
*/
void printAddr(PrintDevice* ptrOBJ,unsigned long addr)
{
    /* 打印前导"0x" */
		ptrOBJ->sendOneByte("0"[0]);
		ptrOBJ->sendOneByte("x"[0]);

    /* 打印地址:格式和十六进制一样 */
    printNum(ptrOBJ,addr, 16);
}

/*
 * 函数名: printStr
 * 函数功能: 打印字符串
 * 参数: 字符串地址
 * 返回值: 无
*/
void printStr(PrintDevice* ptrOBJ,char* str)
{
    int i = 0;

    while (str[i] != '\0')
    {
			ptrOBJ->sendOneByte(str[i++]);
    }
}

/*
 * 函数名: printFloat
 * 函数功能: 打印浮点数
 * 参数: 待打印浮点数
 * 返回值: 无
*/
void printFloat(PrintDevice* ptrOBJ,double f)
{
    int temp;
    if(f < 0){f = -f;ptrOBJ->sendOneByte("-"[0]);}
    /* 先打印整数部分 */
    temp = (int)f;
    if (temp == 0)
    {
        ptrOBJ->sendOneByte("0"[0]);
    }
    printNum(ptrOBJ,temp, 10);

    /* 分隔点 */
    ptrOBJ->sendOneByte("."[0]);

    /* 打印小数部分 */
    f -= temp;
        temp = (int)(f * 1000000);
        printNum(ptrOBJ,temp, 10);
}

