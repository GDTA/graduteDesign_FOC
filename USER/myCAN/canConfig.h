//
// Created by 74354 on 2022/1/21.
//

#ifndef MYODRIVE_CANCONFIG_H
#define MYODRIVE_CANCONFIG_H

#include "can.h"
#include "main.h"

#define THIS_CAN1_ID (0x001U)
#define NODE1_ID     0x12345678

#define NODE2_ID 0x003
#define NODE3_ID 0x004

enum FrameSizeType{
    Standard,
    Expanded
};



class CAN{
public:
    CAN_HandleTypeDef handleCAN;

    void sendDataFrame(FrameSizeType type,uint32_t ID, uint8_t *pData, uint16_t Len) ;
private:
    CAN_TxHeaderTypeDef TxMeg;      // 发送的结构体
    CAN_RxHeaderTypeDef RxMeg;      // 接收的结构体
};

void CAN_User_Init(CAN_HandleTypeDef* hcan);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);
uint8_t CANx_SendNormalData(CAN_HandleTypeDef *hcan, uint16_t ID, uint8_t *pData, uint16_t Len);
void CanReceiveCallback(uint16_t ID,const uint8_t* data,uint8_t len);


#endif //MYODRIVE_CANCONFIG_H
