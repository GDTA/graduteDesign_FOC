/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "spi.h"
#include "adc.h"
#include "tim.h"
#include "can.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
//typedef unsigned char uint8_t;
//typedef unsigned short uint16_t;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define U_DC 12.0f                 // [V]
#define SCALE_Tran  3.30f/4096

#define I_M0_DigToAno(dig) (dig - 2048)*SCALE_Tran/(SAMPLE_REG*M0_DRV8301_I_GAIN)     // [A]
#define I_M1_DigToAno(dig) (dig - 2048)*SCALE_Tran/(SAMPLE_REG*M0_DRV8301_I_GAIN)     // [A]

#define		SPI_DRV8301	        hspi3
#define     SAMPLE_REG          0.0005f           // [Ohm]
#define     M0_DRV8301_I_GAIN   40
#define     M1_DRV8301_I_GAIN   40
#define     M0_driver_timer     htim1
#define     M1_driver_timer     htim8

#define		ADC_M0		        hadc1
#define		ADC_M1		        hadc2
#define     ADC_RegDATA_SIZE    12
#define     ADC_InjDATA_SIZE    12

#define     M0_enc_timer        htim3
#define     M1_enc_timer        htim4
#define     M0_enc_type         Encoder_t::Incremental_ABZ
#define     M1_enc_type         Encoder_t::Incremental_ABZ
#define     M0_enc_cpr          4096*4
#define     M1_enc_cpr          4096*4

#define     AUX_timer         htim2

#define     BOARD_CAN           hcan1


/* USER CODE END Private defines */

typedef     TIM_HandleTypeDef   odTIM;
typedef     SPI_HandleTypeDef   odSPI;
typedef     GPIO_TypeDef        odGPIO_Port;
typedef     uint16_t            odGPIO_Pin;



#define     TIM_1_8_CLOCK_HZ            168000000U
#define     TIM_1_8_PERIOD_CLOCKS       3500U
#define     TIM_1_8_DEADTIME_CLOCKS     20U

#define     TIM_APB1_CLOCK_HZ           84000000U
#define     TIM_APB1_PERIOD_CLOCKS      4096U
#define     TIM_APB1_DEADTIME_CLOCKS    40U
#define     TIM_1_8_RCR                 3U

#define     APB1_Peripheral_CLOCK       45000000
#define     CAN_CLOCK_PRESCALER         7
#define     CAN_BandRate                1000000     // 这个参数根据自己的cubeMX 生成的文件计算


#define current_meas_period ((float)(2*TIM_1_8_PERIOD_CLOCKS)/(float)TIM_1_8_CLOCK_HZ)

#define M0_NCS_Pin GPIO_PIN_13
#define M0_NCS_GPIO_Port GPIOC
#define M1_NCS_Pin GPIO_PIN_14
#define M1_NCS_GPIO_Port GPIOC
//
#define M1_ENC_Z_Pin GPIO_PIN_15
#define M1_ENC_Z_GPIO_Port GPIOC
#define M0_IB_Pin GPIO_PIN_0
#define M0_IB_GPIO_Port GPIOC
#define M0_IC_Pin GPIO_PIN_1
#define M0_IC_GPIO_Port GPIOC
#define M1_IC_Pin GPIO_PIN_2
#define M1_IC_GPIO_Port GPIOC
#define M1_IB_Pin GPIO_PIN_3
#define M1_IB_GPIO_Port GPIOC
#define GPIO_1_Pin GPIO_PIN_0
#define GPIO_1_GPIO_Port GPIOA
#define GPIO_2_Pin GPIO_PIN_1
#define GPIO_2_GPIO_Port GPIOA
#define GPIO_3_Pin GPIO_PIN_2
#define GPIO_3_GPIO_Port GPIOA
#define GPIO_4_Pin GPIO_PIN_3
#define GPIO_4_GPIO_Port GPIOA
#define M1_TEMP_Pin GPIO_PIN_4
#define M1_TEMP_GPIO_Port GPIOA
//
#define AUX_TEMP_Pin GPIO_PIN_5
#define AUX_TEMP_GPIO_Port GPIOA

#define VBUS_S_Pin GPIO_PIN_6
#define VBUS_S_GPIO_Port GPIOA
/* M1_AL */
#define M1_AL_Pin GPIO_PIN_7
#define M1_AL_GPIO_Port GPIOA
//
#define GPIO_5_Pin GPIO_PIN_4
#define GPIO_5_GPIO_Port GPIOC

#define M0_TEMP_Pin GPIO_PIN_5
#define M0_TEMP_GPIO_Port GPIOC

/* M1_BL_CL */
#define M1_BL_Pin GPIO_PIN_0
#define M1_BL_GPIO_Port GPIOB
#define M1_CL_Pin GPIO_PIN_1
#define M1_CL_GPIO_Port GPIOB
//
#define GPIO_6_Pin GPIO_PIN_2
#define GPIO_6_GPIO_Port GPIOB
#define AUX_L_Pin GPIO_PIN_10
#define AUX_L_GPIO_Port GPIOB
#define AUX_H_Pin GPIO_PIN_11
#define AUX_H_GPIO_Port GPIOB
#define EN_GATE_Pin GPIO_PIN_12
#define EN_GATE_GPIO_Port GPIOB

#define M0_AL_Pin GPIO_PIN_13
#define M0_AL_GPIO_Port GPIOB
#define M0_BL_Pin GPIO_PIN_14
#define M0_BL_GPIO_Port GPIOB
#define M0_CL_Pin GPIO_PIN_15
#define M0_CL_GPIO_Port GPIOB

/* M1_AH_BH_CH_ */
#define M1_AH_Pin GPIO_PIN_6
#define M1_AH_GPIO_Port GPIOC
#define M1_BH_Pin GPIO_PIN_7
#define M1_BH_GPIO_Port GPIOC
#define M1_CH_Pin GPIO_PIN_8
#define M1_CH_GPIO_Port GPIOC
//
#define M0_ENC_Z_Pin GPIO_PIN_9
#define M0_ENC_Z_GPIO_Port GPIOC

#define M0_AH_Pin GPIO_PIN_8
#define M0_AH_GPIO_Port GPIOA
#define M0_BH_Pin GPIO_PIN_9
#define M0_BH_GPIO_Port GPIOA
#define M0_CH_Pin GPIO_PIN_10
#define M0_CH_GPIO_Port GPIOA
//
#define GPIO_7_Pin GPIO_PIN_15
#define GPIO_7_GPIO_Port GPIOA

#define NFAULT_Pin GPIO_PIN_2
#define NFAULT_GPIO_Port GPIOD
//
#define GPIO_8_Pin GPIO_PIN_3
#define GPIO_8_GPIO_Port GPIOB

#define M0_ENC_A_Pin GPIO_PIN_4
#define M0_ENC_A_GPIO_Port GPIOB
#define M0_ENC_B_Pin GPIO_PIN_5
#define M0_ENC_B_GPIO_Port GPIOB
#define M1_ENC_A_Pin GPIO_PIN_6
#define M1_ENC_A_GPIO_Port GPIOB
#define M1_ENC_B_Pin GPIO_PIN_7
#define M1_ENC_B_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */



#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
